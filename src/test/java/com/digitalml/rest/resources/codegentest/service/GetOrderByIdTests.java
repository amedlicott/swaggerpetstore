package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.SwaggerPetstore.SwaggerPetstoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetOrderByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetOrderByIdReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetOrderByIdTests {

	@Test
	public void testOperationGetOrderByIdBasicMapping()  {
		SwaggerPetstoreServiceDefaultImpl serviceDefaultImpl = new SwaggerPetstoreServiceDefaultImpl();
		GetOrderByIdInputParametersDTO inputs = new GetOrderByIdInputParametersDTO();
		inputs.setOrderId(0);
		GetOrderByIdReturnDTO returnValue = serviceDefaultImpl.getOrderById(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}