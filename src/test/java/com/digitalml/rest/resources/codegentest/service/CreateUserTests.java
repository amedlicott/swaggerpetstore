package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.SwaggerPetstore.SwaggerPetstoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.CreateUserInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.CreateUserReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateUserTests {

	@Test
	public void testOperationCreateUserBasicMapping()  {
		SwaggerPetstoreServiceDefaultImpl serviceDefaultImpl = new SwaggerPetstoreServiceDefaultImpl();
		CreateUserInputParametersDTO inputs = new CreateUserInputParametersDTO();
		inputs.setBody(new User());
		CreateUserReturnDTO returnValue = serviceDefaultImpl.createUser(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}