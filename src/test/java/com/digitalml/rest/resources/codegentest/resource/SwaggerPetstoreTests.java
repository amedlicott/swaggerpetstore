package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class SwaggerPetstoreTests {

	@Test
	public void testResourceInitialisation() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationUpdatePetNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.updatePet(new Pet());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdatePetNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.updatePet(new Pet());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdatePetAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.updatePet(new Pet());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationAddPetNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.addPet(new Pet());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationAddPetNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.addPet(new Pet());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationAddPetAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.addPet(new Pet());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetUserByNameNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.getUserByName(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetUserByNameNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getUserByName(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetUserByNameAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getUserByName(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationUpdateUserNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.updateUser(org.apache.commons.lang3.StringUtils.EMPTY, new User());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateUserNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.updateUser(org.apache.commons.lang3.StringUtils.EMPTY, new User());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateUserAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.updateUser(org.apache.commons.lang3.StringUtils.EMPTY, new User());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationDeleteUserNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteUser(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteUserNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.deleteUser(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteUserAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.deleteUser(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationFindPetsByStatusNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.findPetsByStatus(new ArrayList&lt;String&gt;());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationFindPetsByStatusNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.findPetsByStatus(new ArrayList&lt;String&gt;());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationFindPetsByStatusAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.findPetsByStatus(new ArrayList&lt;String&gt;());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateUsersWithListInputNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.createUsersWithListInput(new ArrayList&lt;String&gt;());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateUsersWithListInputNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createUsersWithListInput(new ArrayList&lt;String&gt;());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateUsersWithListInputAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createUsersWithListInput(new ArrayList&lt;String&gt;());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationUploadFileNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.uploadFile(0, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUploadFileNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.uploadFile(0, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUploadFileAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.uploadFile(0, null, null);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetInventoryNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.getInventory();
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetInventoryNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getInventory();
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetInventoryAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getInventory();
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationLoginUserNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.loginUser(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationLoginUserNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.loginUser(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationLoginUserAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.loginUser(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateUserNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.createUser(new User());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateUserNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createUser(new User());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateUserAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createUser(new User());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateUsersWithArrayInputNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.createUsersWithArrayInput(new ArrayList&lt;String&gt;());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateUsersWithArrayInputNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createUsersWithArrayInput(new ArrayList&lt;String&gt;());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateUsersWithArrayInputAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createUsersWithArrayInput(new ArrayList&lt;String&gt;());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationFindPetsByTagsNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.findPetsByTags(new ArrayList&lt;String&gt;());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationFindPetsByTagsNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.findPetsByTags(new ArrayList&lt;String&gt;());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationFindPetsByTagsAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.findPetsByTags(new ArrayList&lt;String&gt;());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationPlaceOrderNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.placeOrder(new Order());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationPlaceOrderNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.placeOrder(new Order());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationPlaceOrderAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.placeOrder(new Order());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationLogoutUserNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.logoutUser();
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationLogoutUserNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.logoutUser();
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationLogoutUserAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.logoutUser();
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetPetByIdNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.getPetById(0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetPetByIdNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getPetById(0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetPetByIdAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getPetById(0);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationUpdatePetWithFormNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.updatePetWithForm(0, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdatePetWithFormNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.updatePetWithForm(0, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdatePetWithFormAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.updatePetWithForm(0, null, null);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationDeletePetNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.deletePet(null, 0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeletePetNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.deletePet(null, 0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeletePetAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.deletePet(null, 0);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetOrderByIdNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.getOrderById(0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetOrderByIdNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getOrderById(0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetOrderByIdAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getOrderById(0);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationDeleteOrderNoSecurity() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteOrder(0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteOrderNotAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.deleteOrder(0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteOrderAuthorised() {
		SwaggerPetstoreResource resource = new SwaggerPetstoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.deleteOrder(0);
		Assert.assertEquals(404, response.getStatus());
	}

	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}