package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.SwaggerPetstore.SwaggerPetstoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.CreateUsersWithListInputInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.CreateUsersWithListInputReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateUsersWithListInputTests {

	@Test
	public void testOperationCreateUsersWithListInputBasicMapping()  {
		SwaggerPetstoreServiceDefaultImpl serviceDefaultImpl = new SwaggerPetstoreServiceDefaultImpl();
		CreateUsersWithListInputInputParametersDTO inputs = new CreateUsersWithListInputInputParametersDTO();
		inputs.setBody(new ArrayList&lt;String&gt;());
		CreateUsersWithListInputReturnDTO returnValue = serviceDefaultImpl.createUsersWithListInput(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}