package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.SwaggerPetstore.SwaggerPetstoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UpdateUserInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UpdateUserReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class UpdateUserTests {

	@Test
	public void testOperationUpdateUserBasicMapping()  {
		SwaggerPetstoreServiceDefaultImpl serviceDefaultImpl = new SwaggerPetstoreServiceDefaultImpl();
		UpdateUserInputParametersDTO inputs = new UpdateUserInputParametersDTO();
		inputs.setUsername(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setBody(new User());
		UpdateUserReturnDTO returnValue = serviceDefaultImpl.updateUser(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}