package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.SwaggerPetstore.SwaggerPetstoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.FindPetsByTagsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.FindPetsByTagsReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class FindPetsByTagsTests {

	@Test
	public void testOperationFindPetsByTagsBasicMapping()  {
		SwaggerPetstoreServiceDefaultImpl serviceDefaultImpl = new SwaggerPetstoreServiceDefaultImpl();
		FindPetsByTagsInputParametersDTO inputs = new FindPetsByTagsInputParametersDTO();
		inputs.setTags(new ArrayList&lt;String&gt;());
		FindPetsByTagsReturnDTO returnValue = serviceDefaultImpl.findPetsByTags(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}