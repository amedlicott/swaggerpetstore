package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.SwaggerPetstore.SwaggerPetstoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetPetByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetPetByIdReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetPetByIdTests {

	@Test
	public void testOperationGetPetByIdBasicMapping()  {
		SwaggerPetstoreServiceDefaultImpl serviceDefaultImpl = new SwaggerPetstoreServiceDefaultImpl();
		GetPetByIdInputParametersDTO inputs = new GetPetByIdInputParametersDTO();
		inputs.setPetId(0);
		GetPetByIdReturnDTO returnValue = serviceDefaultImpl.getPetById(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}