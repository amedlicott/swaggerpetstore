package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.SwaggerPetstore.SwaggerPetstoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetUserByNameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetUserByNameReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetUserByNameTests {

	@Test
	public void testOperationGetUserByNameBasicMapping()  {
		SwaggerPetstoreServiceDefaultImpl serviceDefaultImpl = new SwaggerPetstoreServiceDefaultImpl();
		GetUserByNameInputParametersDTO inputs = new GetUserByNameInputParametersDTO();
		inputs.setUsername(org.apache.commons.lang3.StringUtils.EMPTY);
		GetUserByNameReturnDTO returnValue = serviceDefaultImpl.getUserByName(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}