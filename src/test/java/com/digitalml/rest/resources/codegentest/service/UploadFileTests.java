package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.SwaggerPetstore.SwaggerPetstoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UploadFileInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UploadFileReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class UploadFileTests {

	@Test
	public void testOperationUploadFileBasicMapping()  {
		SwaggerPetstoreServiceDefaultImpl serviceDefaultImpl = new SwaggerPetstoreServiceDefaultImpl();
		UploadFileInputParametersDTO inputs = new UploadFileInputParametersDTO();
		inputs.setPetId(0);
		inputs.setAdditionalMetadata(null);
		inputs.setFile(null);
		UploadFileReturnDTO returnValue = serviceDefaultImpl.uploadFile(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}