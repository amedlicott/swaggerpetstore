package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService;
	
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UpdatePetReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UpdatePetReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UpdatePetInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.AddPetReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.AddPetReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.AddPetInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetUserByNameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetUserByNameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetUserByNameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UpdateUserReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UpdateUserReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UpdateUserInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.DeleteUserReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.DeleteUserReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.DeleteUserInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.FindPetsByStatusReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.FindPetsByStatusReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.FindPetsByStatusInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.CreateUsersWithListInputReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.CreateUsersWithListInputReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.CreateUsersWithListInputInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UploadFileReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UploadFileReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UploadFileInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetInventoryReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetInventoryReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetInventoryInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.LoginUserReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.LoginUserReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.LoginUserInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.CreateUserReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.CreateUserReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.CreateUserInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.CreateUsersWithArrayInputReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.CreateUsersWithArrayInputReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.CreateUsersWithArrayInputInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.FindPetsByTagsReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.FindPetsByTagsReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.FindPetsByTagsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.PlaceOrderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.PlaceOrderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.PlaceOrderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.LogoutUserReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.LogoutUserReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.LogoutUserInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetPetByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetPetByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetPetByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UpdatePetWithFormReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UpdatePetWithFormReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.UpdatePetWithFormInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.DeletePetReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.DeletePetReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.DeletePetInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetOrderByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetOrderByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.GetOrderByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.DeleteOrderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.DeleteOrderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.SwaggerPetstoreService.DeleteOrderInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: Swagger Petstore
	 * This is a sample server Petstore server.  You can find out more about Swagger at [http://swagger.io](http://swagger.io)
or on [irc.freenode.net, #swagger](http://swagger.io/irc/).  For this sample, you can use the api key &#x60;special-key&#x60; to
test the authorization filters.
	 *
	 * @author admin
	 * @version 1.0.0
	 *
	 */
	
	@Path("http://petstore-demo-endpoint.execute-api.com/petstore")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class SwaggerPetstoreResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(SwaggerPetstoreResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private SwaggerPetstoreService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.SwaggerPetstore.SwaggerPetstoreServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private SwaggerPetstoreService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof SwaggerPetstoreService)) {
			LOGGER.error(implementationClass + " is not an instance of " + SwaggerPetstoreService.class.getName());
			return null;
		}

		return (SwaggerPetstoreService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: updatePet

	Non-functional requirements:
	*/
	
	@PUT
	@Path("/pet")
	public javax.ws.rs.core.Response updatePet(
		 com.digitalml.rest.resources.codegentest.Pet body) {

		UpdatePetInputParametersDTO inputs = new SwaggerPetstoreService.UpdatePetInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setBody(body);
	
		try {
			UpdatePetReturnDTO returnValue = delegateService.updatePet(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: addPet

	Non-functional requirements:
	*/
	
	@POST
	@Path("/pet")
	public javax.ws.rs.core.Response addPet(
		 com.digitalml.rest.resources.codegentest.Pet body) {

		AddPetInputParametersDTO inputs = new SwaggerPetstoreService.AddPetInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setBody(body);
	
		try {
			AddPetReturnDTO returnValue = delegateService.addPet(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getUserByName

	Non-functional requirements:
	*/
	
	@GET
	@Path("/user/{username}")
	public javax.ws.rs.core.Response getUserByName(
		@PathParam("username")@NotEmpty String username) {

		GetUserByNameInputParametersDTO inputs = new SwaggerPetstoreService.GetUserByNameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setUsername(username);
	
		try {
			GetUserByNameReturnDTO returnValue = delegateService.getUserByName(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateUser
		This can only be done by the logged in user.

	Non-functional requirements:
	*/
	
	@PUT
	@Path("/user/{username}")
	public javax.ws.rs.core.Response updateUser(
		@PathParam("username")@NotEmpty String username,
		 com.digitalml.rest.resources.codegentest.User body) {

		UpdateUserInputParametersDTO inputs = new SwaggerPetstoreService.UpdateUserInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setUsername(username);
		inputs.setBody(body);
	
		try {
			UpdateUserReturnDTO returnValue = delegateService.updateUser(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteUser
		This can only be done by the logged in user.

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/user/{username}")
	public javax.ws.rs.core.Response deleteUser(
		@PathParam("username")@NotEmpty String username) {

		DeleteUserInputParametersDTO inputs = new SwaggerPetstoreService.DeleteUserInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setUsername(username);
	
		try {
			DeleteUserReturnDTO returnValue = delegateService.deleteUser(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findPetsByStatus
		Multiple status values can be provided with comma separated strings

	Non-functional requirements:
	*/
	
	@GET
	@Path("/pet/findByStatus")
	public javax.ws.rs.core.Response findPetsByStatus(
		 List<String> status) {

		FindPetsByStatusInputParametersDTO inputs = new SwaggerPetstoreService.FindPetsByStatusInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setStatus(status);
	
		try {
			FindPetsByStatusReturnDTO returnValue = delegateService.findPetsByStatus(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createUsersWithListInput

	Non-functional requirements:
	*/
	
	@POST
	@Path("/user/createWithList")
	public javax.ws.rs.core.Response createUsersWithListInput(
		 List<String> body) {

		CreateUsersWithListInputInputParametersDTO inputs = new SwaggerPetstoreService.CreateUsersWithListInputInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setBody(body);
	
		try {
			CreateUsersWithListInputReturnDTO returnValue = delegateService.createUsersWithListInput(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: uploadFile

	Non-functional requirements:
	*/
	
	@POST
	@Path("/pet/{petId}/uploadImage")
	public javax.ws.rs.core.Response uploadFile(
		@PathParam("petId")@NotEmpty Integer petId,
		@QueryParam("additionalMetadata") String additionalMetadata,
		@QueryParam("file") String file) {

		UploadFileInputParametersDTO inputs = new SwaggerPetstoreService.UploadFileInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setPetId(petId);
		inputs.setAdditionalMetadata(additionalMetadata);
		inputs.setFile(file);
	
		try {
			UploadFileReturnDTO returnValue = delegateService.uploadFile(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getInventory
		Returns a map of status codes to quantities

	Non-functional requirements:
	*/
	
	@GET
	@Path("/store/inventory")
	public javax.ws.rs.core.Response getInventory() {

		GetInventoryInputParametersDTO inputs = new SwaggerPetstoreService.GetInventoryInputParametersDTO();
		// Prepare and check all input parameters

	
		try {
			GetInventoryReturnDTO returnValue = delegateService.getInventory(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: loginUser

	Non-functional requirements:
	*/
	
	@GET
	@Path("/user/login")
	public javax.ws.rs.core.Response loginUser(
		@QueryParam("username")@NotEmpty String username,
		@QueryParam("password")@NotEmpty String password) {

		LoginUserInputParametersDTO inputs = new SwaggerPetstoreService.LoginUserInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setUsername(username);
		inputs.setPassword(password);
	
		try {
			LoginUserReturnDTO returnValue = delegateService.loginUser(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createUser
		This can only be done by the logged in user.

	Non-functional requirements:
	*/
	
	@POST
	@Path("/user")
	public javax.ws.rs.core.Response createUser(
		 com.digitalml.rest.resources.codegentest.User body) {

		CreateUserInputParametersDTO inputs = new SwaggerPetstoreService.CreateUserInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setBody(body);
	
		try {
			CreateUserReturnDTO returnValue = delegateService.createUser(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createUsersWithArrayInput

	Non-functional requirements:
	*/
	
	@POST
	@Path("/user/createWithArray")
	public javax.ws.rs.core.Response createUsersWithArrayInput(
		 List<String> body) {

		CreateUsersWithArrayInputInputParametersDTO inputs = new SwaggerPetstoreService.CreateUsersWithArrayInputInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setBody(body);
	
		try {
			CreateUsersWithArrayInputReturnDTO returnValue = delegateService.createUsersWithArrayInput(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findPetsByTags
		Muliple tags can be provided with comma separated strings. Use tag1, tag2, tag3
for testing.

	Non-functional requirements:
	*/
	
	@GET
	@Path("/pet/findByTags")
	public javax.ws.rs.core.Response findPetsByTags(
		 List<String> tags) {

		FindPetsByTagsInputParametersDTO inputs = new SwaggerPetstoreService.FindPetsByTagsInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setTags(tags);
	
		try {
			FindPetsByTagsReturnDTO returnValue = delegateService.findPetsByTags(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: placeOrder

	Non-functional requirements:
	*/
	
	@POST
	@Path("/store/order")
	public javax.ws.rs.core.Response placeOrder(
		 com.digitalml.rest.resources.codegentest.Order body) {

		PlaceOrderInputParametersDTO inputs = new SwaggerPetstoreService.PlaceOrderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setBody(body);
	
		try {
			PlaceOrderReturnDTO returnValue = delegateService.placeOrder(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: logoutUser

	Non-functional requirements:
	*/
	
	@GET
	@Path("/user/logout")
	public javax.ws.rs.core.Response logoutUser() {

		LogoutUserInputParametersDTO inputs = new SwaggerPetstoreService.LogoutUserInputParametersDTO();
		// Prepare and check all input parameters

	
		try {
			LogoutUserReturnDTO returnValue = delegateService.logoutUser(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getPetById
		Returns a single pet

	Non-functional requirements:
	*/
	
	@GET
	@Path("/pet/{petId}")
	public javax.ws.rs.core.Response getPetById(
		@PathParam("petId")@NotEmpty Integer petId) {

		GetPetByIdInputParametersDTO inputs = new SwaggerPetstoreService.GetPetByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setPetId(petId);
	
		try {
			GetPetByIdReturnDTO returnValue = delegateService.getPetById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updatePetWithForm

	Non-functional requirements:
	*/
	
	@POST
	@Path("/pet/{petId}")
	public javax.ws.rs.core.Response updatePetWithForm(
		@PathParam("petId")@NotEmpty Integer petId,
		@QueryParam("name") String name,
		@QueryParam("status") String status) {

		UpdatePetWithFormInputParametersDTO inputs = new SwaggerPetstoreService.UpdatePetWithFormInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setPetId(petId);
		inputs.setName(name);
		inputs.setStatus(status);
	
		try {
			UpdatePetWithFormReturnDTO returnValue = delegateService.updatePetWithForm(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deletePet

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/pet/{petId}")
	public javax.ws.rs.core.Response deletePet(
		@QueryParam("api_key") String api_key,
		@PathParam("petId")@NotEmpty Integer petId) {

		DeletePetInputParametersDTO inputs = new SwaggerPetstoreService.DeletePetInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setApi_key(api_key);
		inputs.setPetId(petId);
	
		try {
			DeletePetReturnDTO returnValue = delegateService.deletePet(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getOrderById
		For valid response try integer IDs with value &gt;&#x3D; 1 and &lt;&#x3D; 10. Other values will
generated exceptions

	Non-functional requirements:
	*/
	
	@GET
	@Path("/store/order/{orderId}")
	public javax.ws.rs.core.Response getOrderById(
		@PathParam("orderId")@NotEmpty Integer orderId) {

		GetOrderByIdInputParametersDTO inputs = new SwaggerPetstoreService.GetOrderByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setOrderId(orderId);
	
		try {
			GetOrderByIdReturnDTO returnValue = delegateService.getOrderById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteOrder
		For valid response try integer IDs with positive integer value. Negative or
non-integer values will generate API errors

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/store/order/{orderId}")
	public javax.ws.rs.core.Response deleteOrder(
		@PathParam("orderId")@NotEmpty Integer orderId) {

		DeleteOrderInputParametersDTO inputs = new SwaggerPetstoreService.DeleteOrderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setOrderId(orderId);
	
		try {
			DeleteOrderReturnDTO returnValue = delegateService.deleteOrder(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}